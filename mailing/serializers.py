from rest_framework import serializers
from .models import Client, Dispatch, Message


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ['phone_number', 'mobile_operator_code', 'tag', 'timezone']


class DispatchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dispatch
        fields = ['start_datetime', 'end_datetime', 'message_text', 'mobile_operator_code', 'tag']


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ['creation_datetime', 'is_sent', 'dispatch', 'client']
