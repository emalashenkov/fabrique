from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.db.models import Count, Q

from .models import Client, Dispatch, Message
from .serializers import ClientSerializer, DispatchSerializer, MessageSerializer


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class DispatchViewSet(viewsets.ModelViewSet):
    queryset = Dispatch.objects.all()
    serializer_class = DispatchSerializer


class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer


@api_view(['GET'])
def dispatch_statistics(request):
    """Получение общей статистики по созданным рассылкам и количеству отправленных сообщений по ним с группировкой по
    статусам"""
    statistics = Dispatch.objects.annotate(
        total_messages=Count('message'),
        sent_messages=Count('message', filter=Q(message__is_sent=True)),
        pending_messages=Count('message', filter=Q(message__is_sent=False))
    ).values('id', 'total_messages', 'sent_messages', 'pending_messages',)

    return Response(statistics)


@api_view(['GET'])
def dispatch_detail_statistics(request, dispatch_id):
    """получения детальной статистики отправленных сообщений по конкретной рассылке"""
    statistics = Dispatch.objects.filter(mailing_id=dispatch_id).values(
        'dispatch_id',
        'client_id',
        'is_sent'
    ).annotate(total_messages=Count('id'))

    return Response(statistics)


