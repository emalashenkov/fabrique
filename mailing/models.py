from django.db import models
from django.utils import timezone
from django.core.exceptions import ValidationError
from phonenumber_field.modelfields import PhoneNumberField


class Client(models.Model):
    phone_number = PhoneNumberField(blank=True, unique=True, max_length=11, null=True)  # Assuming 11-digit phone numbers without the country code
    mobile_operator_code = models.CharField(max_length=5, blank=True, null=True)
    tag = models.CharField(max_length=255, blank=True, null=True)
    timezone = models.CharField(max_length=64, blank=True, null=True)

    def __str__(self):
        return f"{self.phone_number}"


class Dispatch(models.Model):
    start_datetime = models.DateTimeField(default=timezone.now, blank=True, null=True)
    end_datetime = models.DateTimeField(blank=True, null=True)
    message_text = models.TextField(blank=True, null=True)
    mobile_operator_code = models.CharField(max_length=5, blank=True, null=True)
    tag = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return f"Dispatch #{self.id}"

    def clean(self):
        if self.start_datetime and self.end_datetime:
            if self.end_datetime < self.start_datetime:
                raise ValidationError("End datetime cannot be earlier than start datetime")


class Message(models.Model):
    creation_datetime = models.DateTimeField(blank=True, null=True)
    is_sent = models.BooleanField(default=False, blank=True, null=True)
    dispatch = models.ForeignKey(Dispatch, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)

    def __str__(self):
        return f"Message #{self.id}"
