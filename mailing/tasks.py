import requests
from celery import shared_task
from django.utils import timezone
from .models import Dispatch, Message, Client


@shared_task
def process_dispatch(tag=None, mobile_operator_code=None):
    # Constructing the filter for the clients
    client_filter = {}
    if tag:
        client_filter['tag'] = tag
    if mobile_operator_code:
        client_filter['mobile_operator_code'] = mobile_operator_code

    # Getting the dispatches that match the filter
    dispatches = Dispatch.objects.filter(**client_filter)

    # Checking the start and end time of each dispatch
    current_time = timezone.now()
    for dispatch in dispatches:
        if dispatch.start_datetime <= current_time <= dispatch.end_datetime:
            # Getting all clients based on the filter criteria
            clients = Client.objects.filter(**client_filter)

            # Sending messages to each client
            for client in clients:
                # Creating a message
                message = Message.objects.create(
                    creation_datetime=current_time,
                    is_sent=False,
                    dispatch=dispatch,
                    client=client
                )

                # Sending the message to the external service
                url = f"https://probe.fbrq.cloud/v1/send/{message.id}"
                headers = {
                    "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MTUyNzE1NzMsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Imh0dHBzOi8vdC5tZS9lbWFsYXNoZW5rb3YifQ.MB2MzAiwxrKLvLnJ1020DAq76tKvuGuc8m65cCF7HrI"
                }
                response = requests.post(url, headers=headers)

                # Updating the message sending status
                message.is_sent = response.ok
                message.save()

        else:
            # Dispatch with a future start time
            # Scheduling the task to be executed at the start time
            process_dispatch.apply_async(args=(tag, mobile_operator_code), eta=dispatch.start_datetime)