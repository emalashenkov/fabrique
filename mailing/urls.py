from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import ClientViewSet, DispatchViewSet, MessageViewSet, dispatch_statistics, dispatch_detail_statistics

router = DefaultRouter()
router.register(r'clients', ClientViewSet, basename='client')
router.register(r'dispatches', DispatchViewSet, basename='dispatch')
router.register(r'messages', MessageViewSet, basename='message')

urlpatterns = [
    path('statistics/', dispatch_statistics, name='dispatch_statistics'),
    path('statistics/<int:dispatch_id>', dispatch_detail_statistics, name='dispatch_detail_statistics'),
]
urlpatterns += router.urls
